<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>User Profile</h1>

<form:form action="/auth/user-profile/" method="POST" modelAttribute="user">

<table width="25%" cellpadding="5" border="0" style="margin-top: 25px">
    <tr>
        <th>ID</th>
        <th>${user.id}</th>
    </tr>
    <tr>
        <th>Login</th>
        <th>${user.login}</th>
    </tr>
    <tr>
        <th>Email</th>
        <th><form:input required="required" type="text" path="email"/></th>
    </tr>
    <tr>
        <th>First Name</th>
        <th><form:input required="required" type="text" path="firstName"/></th>
    </tr>
    <tr>
        <th>Middle Name</th>
        <th><form:input required="required" type="text" path="middleName"/></th>
    </tr>
    <tr>
        <th>Last Name</th>
        <th><form:input required="required" type="text" path="lastName"/></th>
    </tr>
    <tr>
        <th>Role</th>
        <th>${user.roles}</th>
    </tr>
    <tr>
        <th>Locked</th>
        <th>${user.locked}</th>
    </tr>
    <tr class="button">
        <th class="button" colspan="2"><button type="submit">Save Profile</button></th>
    </tr>
</table>
<style>
    tr:not(.button), th:not(.button) {
        border-style: solid;
        border-color: black;
        background: rgb(232, 240, 254);
        border-radius: 6px;
        border-width: 1px;
    }
    .button {
        float: left;
    }
<style/>
</form:form>

<jsp:include page="../include/_footer.jsp"/>