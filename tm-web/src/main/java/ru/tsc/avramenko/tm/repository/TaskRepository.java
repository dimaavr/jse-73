package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Query("SELECT e FROM Task e WHERE e.userId = :userId")
    Collection<Task> findAllByCurrentUserId(
            @Param("userId") final String userId
    );

    @Query("SELECT e FROM Task e WHERE e.id = :id and e.userId = :userId")
    Task findByIdAndUserId(
            @Param("id") final String id,
            @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM Task e WHERE e.id = :id and e.userId = :userId")
    void removeByIdAndUserId(
            @Param("id") @NotNull String id,
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM Task e WHERE e.userId = :userId")
    void removeAllByCurrentUserId(
            @Param("userId") @NotNull String userId
    );

}