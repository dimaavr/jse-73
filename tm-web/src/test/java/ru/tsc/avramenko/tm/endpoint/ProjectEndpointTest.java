package ru.tsc.avramenko.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.commons.configuration.DatabaseConfiguration;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.configuration.WebApplicationConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DataBaseConfiguration.class})
public class ProjectEndpointTest {

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @Autowired
    @NotNull
    private IUserService userService;

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8081/api/projects/";

    @NotNull
    private final Project project1 = new Project("Project1");

    @NotNull
    private final Project project2 = new Project("Project2");

    private static boolean initialized = false;

    @Before
    public void initializeDB() {
        if (!initialized) {
            userService.create("projectEnd", "projectEnd");
            initialized = true;
        }
    }

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("projectEnd", "projectEnd");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(project1);
        save(project2);
    }

    @After
    public void after() {
        clear();
    }

    @SneakyThrows
    private void save(@NotNull final Project project) {
        @NotNull String url = PROJECTS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void clear() {
        @NotNull String url = PROJECTS_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private Project findById(@NotNull final String id) {
        @NotNull String url = PROJECTS_URL + "find/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void find() {
        @NotNull String url = PROJECTS_URL + "find/" + project1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertNotNull(objectMapper.readValue(json, Project.class));
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void findAll() {
        @NotNull String url = PROJECTS_URL + "findAll/";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, Arrays.asList(objectMapper.readValue(json, Project[].class)).size());
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void create() {
        @NotNull final Project project3 = new Project("Project3");
        @NotNull String url = PROJECTS_URL + "create";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project3);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final Project project4 = new Project("Project4");
        @NotNull final Project project5 = new Project("Project5");
        projects.add(project4);
        projects.add(project5);
        @NotNull String url = PROJECTS_URL + "createAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void delete() {
        @NotNull String url = PROJECTS_URL + "delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

}