package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.UserUtil;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class UserServiceTest {

    @NotNull
    private static String USER_ID;

    private static boolean initialized = false;

    @Autowired
    private IUserService userService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Before
    public void initializeDB() {
        if (!initialized) {
            userService.create("userService", "userService");
            initialized = true;
        }
    }

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("userService", "userService");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Test
    @Category(WebCategory.class)
    public void findById() {
        final User user = userService.findById(USER_ID);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(WebCategory.class)
    public void findByLogin() {
        final User user = userService.findByLogin("userService");
        Assert.assertNotNull(user);
    }

    @Test
    @Category(WebCategory.class)
    public void deleteById() {
        final User user = userService.create("test1", "test1");
        userService.removeById(user.getId());
        Assert.assertFalse(userService.isUserExist(user.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void lockUserByLogin() {
        final User user = userService.create("test3", "test3");
        final boolean locked = user.getLocked();
        final boolean lockedNew = userService.lockUserByLogin(user.getLogin()).getLocked();
        Assert.assertNotEquals(locked, lockedNew);
    }

}