package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.model.IProjectService;
import ru.tsc.avramenko.tm.api.service.model.IUserService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.IndexIncorrectException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.model.ProjectRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        @Nullable final User user = userService.findById(userId);
        project.setUser(user);
        project.setName(name);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        @Nullable final User user = userService.findById(userId);
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.findAllById(userId).size() - 1) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId).get(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId).get(index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId).get(index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public Project finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId).get(index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId).get(index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return projectRepository.findAllById(userId);
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    public Project findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.clear();
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<Project> projects) {
        if (projects == null) return;
        projectRepository.saveAll(projects);
    }

}