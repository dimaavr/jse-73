package ru.tsc.avramenko.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tsc.avramenko.tm.endpoint.ProjectEndpoint;
import ru.tsc.avramenko.tm.endpoint.TaskEndpoint;
import javax.xml.ws.Endpoint;

@Configuration
public class WebApplicationConfiguration {

    @Autowired
    private Bus bus;

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskRestEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, taskRestEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}