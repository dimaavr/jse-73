package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;

public interface IProjectService {

    @Nullable Project findById(@Nullable String id);

    @Nullable Collection<Project> findAll();

    void clear();

    @NotNull Project create(@Nullable Project project);

    void removeById(@Nullable String id);

    @NotNull Project save(@NotNull Project project);

}