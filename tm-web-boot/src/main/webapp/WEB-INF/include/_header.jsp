<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Manager</title>
</head>

<style>
    .header a {
        float: left;
        color: black;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px;
        line-height: 25px;
        border-radius: 15px;
    }
    .header a.logo {
        font-size: 25px;
        font-weight: bold;
        border: 2px solid black;
        outline-color: black;
        outline-width: 3px;
        outline-style: solid;
    }

    .header a:hover {
        background-color: #ddd;
        color: black;
    }

    .header-right {
        float: right;
        border: 2px solid black;
        outline-color: black;
        outline-width: 3px;
        outline-style: solid;
        border-radius: 15px;
    }

    h1 {
        color: black;
        text-align: center;
        padding: 12px;
        font-family: Arial, Helvetica, sans-serif;
        text-decoration: none;
        line-height: 25px;
        font-size: 25px;
        font-weight: bold;
    }

    button {
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        color: #fff;
        display:block;
        width:100px;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        padding: 6px 22px;
        margin: 20px auto;
        float: left;
        text-decoration: none;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: background-color 0.1s linear;
        -moz-transition: background-color 0.1s linear;
        -o-transition: background-color 0.1s linear;
        transition: background-color 0.1s linear;
        background-color: rgb( 43, 153, 91 );
        border: 1px solid rgb( 33, 126, 74 );
    }

    button:hover {
        background-color: rgb( 75, 183, 141 );
    }

    div {
        text-align: left;
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        text-align: center;
    }
    th:not(.button), td {
        border-style: solid;
        border-color: black;
        background: #D8E6F3;
    }

</style>

<body>

<div class="header">
    <a class="logo">Task Manager</a>
    <div class="header-right">
        <sec:authorize access="isAuthenticated()">
            <a href="/">Home</a>
            <a href="/projects">Projects</a>
            <a href="/tasks">Tasks</a>
            <a href="/swagger-ui.html#/">Swagger API</a>
    </div>
</div>