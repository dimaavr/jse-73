package ru.tsc.avramenko.tm.exception.system;

import ru.tsc.avramenko.tm.listener.system.HelpListener;
import ru.tsc.avramenko.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    private static String help = new HelpListener().name();

    public UnknownCommandException() {
        super("Incorrect command! Use `" + help + "` for display list of terminal commands.");
    }

}