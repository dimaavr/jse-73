package ru.tsc.avramenko.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractUserListener;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class UserUpdateByLoginListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "user-update-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user info by login.";
    }

    @Override
    @EventListener(condition = "@userUpdateByLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final UserDTO user = adminUserEndpoint.findUserByLogin(session, login);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        adminUserEndpoint.updateUserByLogin(session, login, firstName, lastName, middleName, email);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}